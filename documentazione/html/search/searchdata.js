var indexSectionsWithContent =
{
  0: "acdefghilmnoprstvxy",
  1: "dhiops",
  2: "defgims",
  3: "acefgilmprs",
  4: "acdefghilmnoprstvxy",
  5: "dt",
  6: "dipst",
  7: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "pages"
};

var indexSectionLabels =
{
  0: "Tutto",
  1: "Classi",
  2: "File",
  3: "Funzioni",
  4: "Variabili",
  5: "Tipi enumerati (enum)",
  6: "Valori del tipo enumerato",
  7: "Pagine"
};

