var searchData=
[
  ['cade',['cade',['../structpersonaggio.html#a952fbed713d2ec4e30b2cb22d2608374',1,'personaggio']]],
  ['calcola_5fpunteggio',['calcola_punteggio',['../statistiche_8cc.html#ad236a3603dc2bdc69df10f23c824c8ca',1,'calcola_punteggio(statistiche &amp;stat):&#160;statistiche.cc'],['../statistiche_8h.html#ad236a3603dc2bdc69df10f23c824c8ca',1,'calcola_punteggio(statistiche &amp;stat):&#160;statistiche.cc']]],
  ['coda_5feventi',['coda_eventi',['../structdati__eventi.html#aea0a41e6fe1348fba456d5a8222effaa',1,'dati_eventi']]],
  ['col_5fdx',['COL_DX',['../dati_8cc.html#a47cd91a3159615234c62279da368531b',1,'COL_DX():&#160;dati.cc'],['../fileIO_8cc.html#a47cd91a3159615234c62279da368531b',1,'COL_DX():&#160;dati.cc'],['../main_8cc.html#a47cd91a3159615234c62279da368531b',1,'COL_DX():&#160;dati.cc']]],
  ['col_5fhud',['COL_HUD',['../dati_8cc.html#a8d06e6cc14b25b172283ec8e670626f7',1,'COL_HUD():&#160;dati.cc'],['../fileIO_8cc.html#a8d06e6cc14b25b172283ec8e670626f7',1,'COL_HUD():&#160;dati.cc'],['../main_8cc.html#a8d06e6cc14b25b172283ec8e670626f7',1,'COL_HUD():&#160;dati.cc']]],
  ['col_5fsx',['COL_SX',['../dati_8cc.html#a9cdfd60978604a36c70391eca5f667a8',1,'COL_SX():&#160;dati.cc'],['../fileIO_8cc.html#a9cdfd60978604a36c70391eca5f667a8',1,'COL_SX():&#160;dati.cc'],['../main_8cc.html#a9cdfd60978604a36c70391eca5f667a8',1,'COL_SX():&#160;dati.cc']]],
  ['collidono',['collidono',['../dati_8cc.html#ac085a64f08d91ed53c5901515602cb83',1,'collidono(personaggio p, oggetto o):&#160;dati.cc'],['../dati_8cc.html#a3c0a1351f0abfe22f6b2721901ea6fae',1,'collidono(personaggio p1, personaggio p2):&#160;dati.cc'],['../dati_8cc.html#a14fca04c73655a445e4a0e37612d953f',1,'collidono(oggetto o1, oggetto o2):&#160;dati.cc']]]
];
