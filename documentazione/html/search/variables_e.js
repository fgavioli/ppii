var searchData=
[
  ['salto',['salto',['../eventi_8h.html#a100d22a03acd7c0680af684c9858cec0',1,'eventi.h']]],
  ['serve_5fdisegno',['serve_disegno',['../structdati__schermo.html#a5ee6fddcbf1eb9ebf58c74b2ed173578',1,'dati_schermo']]],
  ['sfondo',['sfondo',['../structdati__schermo.html#aa107edb1b1b95a045688784c51d4fdf2',1,'dati_schermo']]],
  ['sfondo_5f1',['SFONDO_1',['../dati_8cc.html#a4bf17975520f6f0019e017febc3d62f0',1,'SFONDO_1():&#160;dati.cc'],['../fileIO_8cc.html#a4bf17975520f6f0019e017febc3d62f0',1,'SFONDO_1():&#160;dati.cc'],['../main_8cc.html#a4bf17975520f6f0019e017febc3d62f0',1,'SFONDO_1():&#160;dati.cc']]],
  ['spada_5fdx',['SPADA_DX',['../dati_8cc.html#a8d8a4d3db64410747ce2299c0c05c2bb',1,'SPADA_DX():&#160;dati.cc'],['../fileIO_8cc.html#a8d8a4d3db64410747ce2299c0c05c2bb',1,'SPADA_DX():&#160;dati.cc']]],
  ['spada_5fsx',['SPADA_SX',['../dati_8cc.html#ada20e00b826a92457567e8dc9a3666c2',1,'SPADA_SX():&#160;dati.cc'],['../fileIO_8cc.html#ada20e00b826a92457567e8dc9a3666c2',1,'SPADA_SX():&#160;dati.cc']]],
  ['spdc_5fhp',['SPDC_HP',['../dati_8h.html#a40eb31d733bd7b57c0e77016eb4b9a5c',1,'dati.h']]],
  ['sprite',['sprite',['../structoggetto.html#a8dc23f6c3ed84f53db529288b8e3ed1a',1,'oggetto::sprite()'],['../structpersonaggio.html#abdfa7cd274f0f098d123ce830688fc7a',1,'personaggio::sprite()']]],
  ['stat',['stat',['../dati_8cc.html#aaf37f3a2ad02610bb8b0ed4b392e2bb2',1,'stat():&#160;dati.cc'],['../grafica_8cc.html#aaf37f3a2ad02610bb8b0ed4b392e2bb2',1,'stat():&#160;dati.cc'],['../main_8cc.html#aaf37f3a2ad02610bb8b0ed4b392e2bb2',1,'stat():&#160;dati.cc']]]
];
