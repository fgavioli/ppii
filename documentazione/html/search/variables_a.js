var searchData=
[
  ['narmi',['narmi',['../structdati__schermo.html#a7190ae75d41c95580e05fb352ab911f4',1,'dati_schermo']]],
  ['nemici_5feliminati',['nemici_eliminati',['../structstatistiche.html#a2de1de9b2da0b094a4188d2cdbe77d50',1,'statistiche']]],
  ['nnpc',['nnpc',['../structdati__schermo.html#a8d6cbf4478eb0b651ef903c28822d11b',1,'dati_schermo']]],
  ['nome',['nome',['../structhighscore.html#a05f11acff810fdcd3e04c762b300f708',1,'highscore']]],
  ['npc_5fhud',['NPC_HUD',['../dati_8cc.html#ad830900f471fd32a68116ddbd9d4c8bf',1,'NPC_HUD():&#160;dati.cc'],['../fileIO_8cc.html#ad830900f471fd32a68116ddbd9d4c8bf',1,'NPC_HUD():&#160;dati.cc'],['../main_8cc.html#ad830900f471fd32a68116ddbd9d4c8bf',1,'NPC_HUD():&#160;dati.cc']]],
  ['npc_5fogg',['NPC_OGG',['../dati_8h.html#af5822354f8a12b7ae3557ae531db4ec9',1,'dati.h']]],
  ['num_5foggetti',['num_oggetti',['../structpersonaggio.html#a24fd1c15cb52caf1ed9d0d190ddbf040',1,'personaggio']]],
  ['nvite',['nvite',['../structdati__schermo.html#aeb26dc1a6d8a5f3012facc88a6b93a02',1,'dati_schermo']]]
];
