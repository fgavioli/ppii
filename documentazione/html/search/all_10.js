var searchData=
[
  ['vel_5fogg_5fmelee',['VEL_OGG_MELEE',['../dati_8h.html#af714aefc7c3f08e25431c67437ad2ea6',1,'dati.h']]],
  ['vel_5fogg_5franged',['VEL_OGG_RANGED',['../dati_8h.html#a12125497ac387758abd7648f403fd3ea',1,'dati.h']]],
  ['vel_5fsalto',['VEL_SALTO',['../main_8cc.html#aa3eab284bc73206a0f0b3358f9c57d10',1,'main.cc']]],
  ['vel_5fx',['vel_x',['../structoggetto.html#a307e101989ae2fdb1cac11145f4d9ced',1,'oggetto::vel_x()'],['../structpersonaggio.html#a159b2bc7ed0dadb4d00fca244d4cbad5',1,'personaggio::vel_x()']]],
  ['vel_5fy',['vel_y',['../structoggetto.html#a1ec5250043959c6863f77e38fb900652',1,'oggetto::vel_y()'],['../structpersonaggio.html#aced71bf6aadb1d9b5cca24b8e1c89774',1,'personaggio::vel_y()']]],
  ['visibile',['visibile',['../structinformazioni__disegno.html#a220a63711084555f2dd2e5de1daa76b4',1,'informazioni_disegno']]]
];
