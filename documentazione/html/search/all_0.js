var searchData=
[
  ['act_5fia_5fps',['ACT_IA_PS',['../main_8cc.html#a86b9684986037cdfc0d5ebdc7aaf42eb',1,'main.cc']]],
  ['aggiorna_5fvelocita_5fx',['aggiorna_velocita_x',['../movimento_8cc.html#a11a2ef571913a39c2db398e334282bf4',1,'aggiorna_velocita_x(dati_gioco &amp;dati):&#160;movimento.cc'],['../movimento_8h.html#a11a2ef571913a39c2db398e334282bf4',1,'aggiorna_velocita_x(dati_gioco &amp;dati):&#160;movimento.cc']]],
  ['aggiorna_5fvelocita_5fy',['aggiorna_velocita_y',['../movimento_8cc.html#a5a8c1b9e91508d53cd88d60028c5e1af',1,'aggiorna_velocita_y(dati_gioco &amp;dati):&#160;movimento.cc'],['../movimento_8h.html#a5a8c1b9e91508d53cd88d60028c5e1af',1,'aggiorna_velocita_y(dati_gioco &amp;dati):&#160;movimento.cc']]],
  ['aggps',['AGGPS',['../main_8cc.html#ac0486efcdb28952dcf366e29674eb8a8',1,'main.cc']]],
  ['alt_5fogg',['ALT_OGG',['../dati_8h.html#abc359d0af03952c37075b13ce8d859c3',1,'dati.h']]],
  ['alt_5fsprite',['ALT_SPRITE',['../dati_8h.html#aca045ca2840c71baaec89a52389e5472',1,'dati.h']]],
  ['altezza',['altezza',['../structdati__schermo.html#a840208b5dabbb3ee4b79d3ae61bb4a42',1,'dati_schermo']]],
  ['anim_5ffps',['ANIM_FPS',['../main_8cc.html#a3b713e8ef41a289f0c6063c940fb2b63',1,'main.cc']]],
  ['arma',['arma',['../structpersonaggio.html#a5f5b1610f42cebb84c40f37a683a3299',1,'personaggio']]],
  ['att_5fdist',['att_dist',['../eventi_8h.html#a8c6ba2707b3dcc2c5afc25d553828263',1,'eventi.h']]],
  ['att_5fmelee',['att_melee',['../eventi_8h.html#a59d660c0b3c1d631c585c08d87548caa',1,'eventi.h']]],
  ['attacca',['attacca',['../dati_8cc.html#ad464eb9c664befb6046c71477469f71b',1,'attacca(personaggio &amp;p):&#160;dati.cc'],['../dati_8h.html#ad464eb9c664befb6046c71477469f71b',1,'attacca(personaggio &amp;p):&#160;dati.cc']]],
  ['avanza_5fanimazioni',['avanza_animazioni',['../grafica_8cc.html#a222311690c28f60e94515edd5f4d4efd',1,'avanza_animazioni(dati_schermo &amp;schermo, dati_gioco &amp;gioco):&#160;grafica.cc'],['../grafica_8h.html#a222311690c28f60e94515edd5f4d4efd',1,'avanza_animazioni(dati_schermo &amp;schermo, dati_gioco &amp;gioco):&#160;grafica.cc']]]
];
