#include <iostream>
#include <fstream>

using namespace std;

#ifndef STATISTICHE
#define STATISTICHE

const int PT_PER_ELM      = 200;
const int PT_PER_SEC      =  50;
const int PT_PER_DAN_CMP  =  20;
const int PT_PER_DAN_SPD  =  75;
const int PT_PER_DAN_OGG  = 125;

/** Struttura dati che immagazina tutte le
  * statistiche della partita corrente.
  */
struct statistiche
{
    int nemici_eliminati;           /**< Numero di nemici eliminati durante la partita*/
    int oggetti_lanciati_pg;        /**< Numero di oggetti lanciati dal personaggio durante la partita*/
    double durata_partita;          /**< Durata in secondi della partita*/
    int danni_ricevuti_cmp;         /**< Danni ricevuti da compenetrazione con nemici*/ 
    int danni_ricevuti_spada;       /**< Danni ricevuti da armi ravvicinate*/
    int punteggio;                  /**< Punteggio calcolato alla fine della partita*/
};

/** Struttura dati che associa un nome di
  * 3 caratteri ad un punteggio numerico intero.
  */
struct highscore
{
    char nome[4] = "   ";
    int punteggio;
};

#endif

/** Funzione che inizializza a 0 tutti i contatori delle statistiche
 */
statistiche inizializza_statistiche();

/** Funzione che ritorna la statistica richiesta in base al codice fornito
 * come parametro di input.
 */
double stats(statistiche stat, int i);

/** Funzione che calcola il punteggio finale in base 
 * a: numero di nemici eliminati, tempo di gioco e 
 * tipo di danni ricevuti
 */
void calcola_punteggio(statistiche &stat);