#include "fileIO.h"

extern ALLEGRO_BITMAP *PG_SPRITE;
extern ALLEGRO_BITMAP *COL_DX;
extern ALLEGRO_BITMAP *COL_SX;
extern ALLEGRO_BITMAP *LAN_DX;
extern ALLEGRO_BITMAP *LAN_SX;
extern ALLEGRO_BITMAP *SFONDO_1;
extern ALLEGRO_BITMAP *SPADA_DX;
extern ALLEGRO_BITMAP *SPADA_SX;
extern ALLEGRO_BITMAP *COL_HUD;
extern ALLEGRO_BITMAP *HP_HUD;
extern ALLEGRO_BITMAP *NPC_HUD;
extern ALLEGRO_BITMAP *PAUSA;
extern ALLEGRO_BITMAP *DIR_DX;
extern ALLEGRO_BITMAP *DIR_SX;
extern ALLEGRO_BITMAP *IMG_SPDC;
extern ALLEGRO_BITMAP *IMG_INSG;
extern ALLEGRO_BITMAP *IMG_TANK;


bool load_immagini(const char img_sfondo[], const char img_pg[], const char img_col_dx[], 
                    const char img_col_sx[], const char img_lan_dx[], const char img_lan_sx[],
                    const char img_spada_dx[], const char img_spada_sx[], const char img_col_hud[], 
                    const char img_hp_hud[], const char img_npc_hud[], const char img_pausa[], 
                    const char img_dir_dx[], const char img_dir_sx[], const char img_spdc[],
                    const char img_tank[], const char img_insg[])
{
    SFONDO_1 = al_load_bitmap(img_sfondo);
    PG_SPRITE = al_load_bitmap(img_pg);
    COL_DX = al_load_bitmap(img_col_dx);
    COL_SX = al_load_bitmap(img_col_sx);
    LAN_DX = al_load_bitmap(img_lan_dx);
    LAN_SX = al_load_bitmap(img_lan_sx);
    SPADA_DX = al_load_bitmap(img_spada_dx);
    SPADA_SX = al_load_bitmap(img_spada_sx);
    HP_HUD = al_load_bitmap(img_hp_hud);
    COL_HUD = al_load_bitmap(img_col_hud);
    NPC_HUD = al_load_bitmap(img_npc_hud);
    PAUSA = al_load_bitmap(img_pausa);
    DIR_DX = al_load_bitmap(img_dir_dx);
    DIR_SX = al_load_bitmap(img_dir_sx);
    IMG_SPDC = al_load_bitmap(img_spdc);
    IMG_TANK = al_load_bitmap(img_tank);
    IMG_INSG = al_load_bitmap(img_insg);

    if(SFONDO_1 == NULL)
        return false;
    if(PG_SPRITE == NULL)
        return false;
    if(COL_DX == NULL)
        return false;
    if(COL_SX == NULL)
        return false;
    if(LAN_DX == NULL)
        return false;
    if(LAN_SX == NULL)
        return false;
    if(SPADA_DX == NULL)
        return false;
    if(SPADA_SX == NULL)
        return false;
    if(HP_HUD == NULL)
        return false;
    if(COL_HUD == NULL)
        return false;
    if(NPC_HUD == NULL)
        return false;
    if(PAUSA == NULL)
        return false;
    if(DIR_DX == NULL)
        return false;
    if(DIR_SX == NULL)
        return false;
    if(IMG_SPDC == NULL)
        return false;
    if(IMG_INSG == NULL)
        return false;
    if(IMG_TANK == NULL)
        return false;

    return true;
}

void sort(highscore *h, int N)
{   
    for(int i = 0; i < N; i++)
    {
        int attuale = h[i].punteggio;
        int j;
        for(j = i; j > 0 && h[j - 1].punteggio < attuale; j--)
        {
            highscore c = h[j];
            h[j] = h[j-1];
            h[j-1] = c;
        }    
    }
}

highscore* leggi_highscores(int &i)
{    
    ifstream in("highscores.sav");
    if(!in)
        return NULL;
    
    highscore *h = new highscore[10];
    i = 0;
    
    while (!in.eof() && i < 10)
    {
        in >> h[i].nome >> h[i].punteggio;
        i++;
    }

    if(in.eof())
        i--;

    sort(h, i);

    in.close();
    return h;
}

bool salva_risultati(highscore h)
{
    int i = 10;
    highscore* hs = leggi_highscores(i);
    hs[i] = h;
    i++;
    sort(hs, i);

    ofstream of("highscores.sav");
    if(!of)
        return false;
    for(int j = 0; hs[j].punteggio != 0; j++)
        of << hs[j].nome << " " << hs[j].punteggio << endl;
    of.flush();
    of.close();
    return true;
}