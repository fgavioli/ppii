#include "dati.h"

ALLEGRO_BITMAP *PG_SPRITE;      /**< Immagine del personaggio*/
ALLEGRO_BITMAP *COL_DX;         /**< Immagine del coltello rivolto a destra*/
ALLEGRO_BITMAP *COL_SX;         /**< Immagine del coltello rivolto a sinistra*/
ALLEGRO_BITMAP *LAN_DX;         /**< Immagine della lancia rivolta a destra*/
ALLEGRO_BITMAP *LAN_SX;         /**< Immagine della lancia rivolta a sinistra*/
ALLEGRO_BITMAP *SFONDO_1;       /**< Immagine dello sfondo di gioco*/
ALLEGRO_BITMAP *SPADA_DX;       /**< Immagine della spada rivolta a destra*/
ALLEGRO_BITMAP *SPADA_SX;       /**< Immagine della spada rivolta a sinistra*/
ALLEGRO_BITMAP *COL_HUD;        /**< Immagine del contatore di coltelli rimanenti*/
ALLEGRO_BITMAP *HP_HUD;         /**< Immagine del contatore di vite rimanenti*/
ALLEGRO_BITMAP *NPC_HUD;        /**< Immagine del contatore di nemici rimanenti*/
ALLEGRO_BITMAP *PAUSA;          /**< Immagine della schermata di pausa*/
ALLEGRO_BITMAP *DIR_DX;         /**< Immagine della freccia direzionale rivolta a destra*/
ALLEGRO_BITMAP *DIR_SX;         /**< Immagine della freccia direzionale rivolta a sinistra*/
ALLEGRO_BITMAP *IMG_SPDC;       /**< Immagine dello spadaccino*/
ALLEGRO_BITMAP *IMG_INSG;       /**< Immagine dell'inseguitore*/
ALLEGRO_BITMAP *IMG_TANK;       /**< Immagine del png tank*/

statistiche stat;

personaggio inizializza_personaggio(int x, int y, int rotazione, tipo_t tipo, direzione_t dir)
{
    personaggio pg;
    pg.x = x;
    pg.y = y;
    pg.vel_x = 0;
    pg.vel_y = 0;
    pg.direzione = dir;
    pg.in_movim_dx = false;
    pg.in_movim_sx = false;
    pg.in_salto = false;
    pg.cade = false;
    pg.sprite.visibile = true;
    pg.tipo = tipo;
    pg.rim = false;
    if(tipo == PRINCIPALE)
    {
        pg.num_oggetti = PG_OGG;
        pg.sprite.immagine = PG_SPRITE;
        pg.hp_totali = pg.hp = PG_HP;
    }
    else if(tipo == SPADACCINO)
    {
        pg.sprite.immagine = IMG_SPDC;
        pg.hp_totali = pg.hp = SPDC_HP;
    }
    else if(tipo == INSEGUITORE)
    {
        pg.sprite.immagine = IMG_INSG;
        pg.hp_totali = pg.hp = INSG_HP;
    }
    else
    {
        pg.sprite.immagine = IMG_TANK;
        pg.hp_totali = pg.hp = TANK_HP;
    }
    if(dir == DX)
        pg.dir_img = DIR_DX;
    else
        pg.dir_img = DIR_SX;
    pg.arma = inizializza_oggetto(pg.x + OFFSET_LARG_MELEE, pg.y + OFFSET_ALT_MELEE, VEL_OGG_MELEE, 0, pg.direzione, pg, SPADA_DX);
    pg.arma.sprite.visibile = false;
    return pg;
}

void attacca(personaggio &p)
{
    if(!p.in_attacco)
    {
        p.arma.direzione = p.direzione;
        p.arma.x = p.x + (p.arma.vel_x*p.arma.fase_attacco);
        p.arma.y = p.y + OFFSET_ALT_MELEE;
        if(p.direzione == DX)
        {
            p.arma.sprite.immagine = SPADA_DX;
            p.arma.vel_x = VEL_OGG_MELEE;
        }
        else
        {
            p.arma.sprite.immagine = SPADA_SX;
            p.arma.vel_x = -VEL_OGG_MELEE;
        }
        p.arma.sprite.visibile = true;
        p.arma.fase_attacco = 0;
        p.arma.in_uscita = true;    
        p.in_attacco = true;
    }
}

void spara(dati_gioco &dati, personaggio &p)
{
    if(p.num_oggetti > 0)
    {
        p.num_oggetti--;
        oggetto ogg;
        if(p.direzione == DX)
        {
            if(&p == &dati.personaggi.front())
                ogg = inizializza_oggetto(p.x, p.y + OFFSET_ALT_MELEE, VEL_OGG_RANGED, 0, p.direzione, p, COL_DX);
            else
                ogg = inizializza_oggetto(p.x, p.y + OFFSET_ALT_MELEE, VEL_OGG_RANGED, 0, p.direzione, p, LAN_DX);
        }
        else if(p.direzione == SX)
        {
            if(&p == &dati.personaggi.front())
                ogg = inizializza_oggetto(p.x, p.y + OFFSET_ALT_MELEE, VEL_OGG_RANGED, 0, p.direzione, p, COL_SX);
            else
                ogg = inizializza_oggetto(p.x, p.y + OFFSET_ALT_MELEE, VEL_OGG_RANGED, 0, p.direzione, p, LAN_SX);
        }
        dati.oggetti.push_front(ogg);
        if(p.tipo == PRINCIPALE)
            stat.oggetti_lanciati_pg++;
    }
}

tipo_t random_tipo(int inseguitori, int spadaccini, int tank)
{
    int probspad = 0;
    int probins = 0;
    int probtank = 0;

    if(tank > 1)
    {
        probspad = 45;
        probins = 45;
        probtank = 0;
        //Probabilita' di non generazione = 10%
    }
    else if(inseguitori > (spadaccini + 1))
    {
        probspad = 55;
        probins = 20;
        probtank = 15;
        //Probabilita' di non generazione = 10%
    }
    else
    {
        probspad = 40;
        probins = 40;
        probtank = 20;
        //Probabilita' di non generazione = 0%
    }
    
    probins += probspad;
    probtank += probins;

    int r = (rand() % 100) + 1;
    
    if(r < probspad)
        return SPADACCINO;
    else if(r < probins)
        return INSEGUITORE;
    else if(r < probtank)
        return TANK;
    else
        return PRINCIPALE;
}

void genera_png(dati_gioco &dati, int y_terreno)
{
    int x = 0; 
    int y = 0;
    int inseguitori = 0;
    int spadaccini = 0;
    int tank = 0;

    //Conteggio dei tipi di personaggio a scopo di
    //mantenere il gioco giocabile senza generare troppi
    //personaggi uguali.    
    for(personaggio p : dati.personaggi)
    {
        if(p.tipo == SPADACCINO)
            spadaccini++;
        else if (p.tipo == INSEGUITORE)
            inseguitori++;
        else if (p.tipo == TANK)
            tank++;
    }

    do
    {
        x = rand() % (1365 - LARG_SPRITE) + 1;
        y = y_terreno - ALT_SPRITE;
    }while(!(x < (dati.personaggi.front().x - 400) || x > (dati.personaggi.front().x + LARG_SPRITE + 350)));
    
    tipo_t tipo = random_tipo(inseguitori, spadaccini, tank);
    if(tipo == PRINCIPALE)
        return;

    direzione_t dir = DX;
    if(x > dati.personaggi.front().x)
        dir = SX;

    personaggio p = inizializza_personaggio(x, y, 0, tipo, dir);
    dati.personaggi.push_back(p);
}

oggetto inizializza_oggetto(int x, int y, int vel_x, int vel_y, direzione_t dir, personaggio &parent, ALLEGRO_BITMAP *sprite)
{
    oggetto ogg;
    ogg.padre = &parent;
    ogg.x = x;
    ogg.y = y;
    ogg.vel_x = vel_x;
    if(dir == SX)
        ogg.vel_x = -vel_x;
    ogg.vel_y = vel_y;
    ogg.sprite.immagine = sprite;
    ogg.direzione = dir;
    ogg.sprite.visibile = true;
    ogg.fase_attacco = 0;
    ogg.in_uscita = true;
    ogg.rim = false;
    return ogg;
}

bool collidono(personaggio p, oggetto o)
{
    int Ax, Bx, Ay, Cy;
    Ax = p.x + OFFSET_COLL;
    Bx = p.x + LARG_SPRITE - OFFSET_COLL;
    Ay = p.y + OFFSET_COLL;
    Cy = p.y + ALT_SPRITE - OFFSET_COLL;
    if((o.x > Ax && o.x < Bx) || (o.x + LARG_OGG > Ax && o.x + LARG_OGG < Bx))
    {
        if(o.y + ALT_OGG > Ay && o.y + ALT_OGG < Cy)
            return true;
    }
    return false;
}

bool collidono(personaggio p1, personaggio p2)
{
    if(abs(p1.x - p2.x) < (LARG_SPRITE - OFFSET_COLL) && abs(p1.y - p2.y) < (ALT_SPRITE - OFFSET_COLL))
        return true;
    return false;
}

bool collidono(oggetto o1, oggetto o2)
{
    if(abs(o2.x - o1.x) < LARG_OGG && abs(o2.y - o1.y) < ALT_OGG)
            return true;
    return false;
}

bool risolvi_collisioni_pg_pg(list<personaggio> &personaggi)
{
    list<personaggio>::iterator p = personaggi.begin();
    for(list<personaggio>::iterator j = personaggi.begin(); j != personaggi.end(); )
    {
        if(collidono(*p, *j) && j != p)
        {
            #ifdef DBG
            cout << "Ricevuto 1 danno per compenetrazione con nemico" << endl;
            #endif
            j->hp--;
            p->hp--;
            stat.danni_ricevuti_cmp++;
            if(p->hp <= 0)
                return true;
            if(j->hp <= 0)
            {
                j = personaggi.erase(j);
                j--;
                stat.nemici_eliminati++;
                continue;
            }
        }

        if(p->arma.sprite.visibile && collidono(*j, p->arma) && j != p)
        {
            j->hp--;
            p->arma.sprite.visibile = false;
            if(j->hp <= 0)
            {
                j = personaggi.erase(j);
                j--;
                stat.nemici_eliminati++;
                continue;
            }
        }

        if(j->arma.sprite.visibile && collidono(*p, j->arma) && j != p)
        {
            if(j->tipo == TANK)
            {
                p->hp -= 2;
                stat.danni_ricevuti_spada ++;
                if(p->hp != -1)
                    stat.danni_ricevuti_spada ++;
                #ifdef DBG
                    cout << "Ricevuti due danni da arma ravvicinata tank" << endl;    
                #endif
            }
            else
            {
                p->hp--;
                stat.danni_ricevuti_spada++;
                #ifdef DBG
                    cout << "Ricevuti due danni da arma ravvicinata tank" << endl;    
                #endif
            }
            j->arma.sprite.visibile = false;
            if(p->hp <= 0)
                return true;
        }
        j++;
    }

    return false;
}


bool risolvi_collisioni_ogg_pg(list<oggetto> &oggetti, list<personaggio> &personaggi)
{
    for(oggetto &o : oggetti)
    {
        for(personaggio &p : personaggi)
        {
            if(p.tipo != PRINCIPALE && o.sprite.visibile && collidono(p, o))
            {
                #ifdef DBG
                cout << "Nemico eliminato con arma da distanza" << endl;
                #endif
                assert(&o != &*(oggetti.end()));
                p.hp--;
                if(p.hp <= 0)
                {
                    p.rim = true;
                    stat.nemici_eliminati++;
                }
                o.sprite.visibile = false;
                o.rim = true;
                assert(o.rim);
                break;
            }
        }
    }
    oggetti.remove_if([](oggetto o){return o.rim;});
    oggetti.remove_if([](oggetto o){return !(o.sprite.visibile);});
    personaggi.remove_if([](personaggio p){return p.rim;});
    return false;
}

//Riscrivere
void risolvi_collisioni_ogg_ogg(list<oggetto> &oggetti)
{
    for(list<oggetto>::iterator o1 = oggetti.begin(); o1 != oggetti.end(); o1++)
    {
        for(list<oggetto>::iterator o2 = next(o1, 1); o2 != oggetti.end(); o2++)
        {
            if(o1->sprite.visibile && o2->sprite.visibile && collidono(*o1, *o2) && &(*o1) != &(*o2))
            {
                #ifdef DBG
                cout << "Oggetti cancellati per collisione tra di loro" << endl;
                #endif
                o1->rim = true;
                o1->sprite.visibile = false;
                o2->sprite.visibile = false;
                o2->rim = true;
                assert(o1->rim);
                assert(o2->rim);
                break;
            }
        }
    }
    oggetti.remove_if([](oggetto o){return o.rim;});
    oggetti.remove_if([](oggetto o){return !(o.sprite.visibile);});
}