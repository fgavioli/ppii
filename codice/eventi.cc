#include "eventi.h"

extern ALLEGRO_BITMAP *PAUSA;
extern ALLEGRO_BITMAP *DIR_SX;
extern ALLEGRO_BITMAP *DIR_DX;

void registra_sorgenti_eventi(dati_eventi &eventi, dati_schermo &schermo)
{
    al_register_event_source(eventi.coda_eventi, al_get_keyboard_event_source());
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_fps));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_anim));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_mov_pers));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_mov_ogg));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_agg));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_salto));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_gen_nemici));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_gen_armi_pg));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_gen_armi_png));
    al_register_event_source(eventi.coda_eventi, al_get_timer_event_source(eventi.timer_ia));
	al_register_event_source(eventi.coda_eventi, al_get_display_event_source(schermo.display));
}

void start_timers(dati_eventi &eventi)
{
    al_start_timer(eventi.timer_fps);
    al_start_timer(eventi.timer_anim);
    al_start_timer(eventi.timer_mov_pers);
    al_start_timer(eventi.timer_mov_ogg);
    al_start_timer(eventi.timer_agg);
    al_start_timer(eventi.timer_salto);
    al_start_timer(eventi.timer_gen_nemici);
    al_start_timer(eventi.timer_gen_armi_pg);
    al_start_timer(eventi.timer_gen_armi_png);
    al_start_timer(eventi.timer_ia);
}

void stop_timers(dati_eventi &eventi)
{
    al_stop_timer(eventi.timer_fps);
    al_stop_timer(eventi.timer_anim);
    al_stop_timer(eventi.timer_mov_pers);
    al_stop_timer(eventi.timer_mov_ogg);
    al_stop_timer(eventi.timer_agg);
    al_stop_timer(eventi.timer_salto);
    al_stop_timer(eventi.timer_gen_nemici);
    al_stop_timer(eventi.timer_gen_armi_pg);
    al_stop_timer(eventi.timer_gen_armi_png);
    al_stop_timer(eventi.timer_ia);
}

void in_pausa(dati_eventi &eventi, bool &pausa)
{
    stop_timers(eventi);
    
    al_draw_bitmap(PAUSA, 0, 0, 0);
    al_flip_display();
    
    pausa = true;
    
    #ifdef DBG 
    cout << "In pausa" << endl; 
    #endif
}

void fuori_pausa(dati_eventi &eventi, bool &pausa)
{
    start_timers(eventi);
    
    pausa = false;
    
    #ifdef DBG 
    cout << "Fuori pausa" << endl;
    #endif
}

bool gestore_tasto_giu(dati_eventi &eventi, ALLEGRO_EVENT *evt, dati_gioco &dati, bool &pausa)
{
    switch(evt->keyboard.keycode)
    {
        case mov_sx:
            dati.personaggi.front().in_movim_sx = true;
            dati.personaggi.front().in_movim_dx = false;
            dati.personaggi.front().direzione = SX;
            dati.personaggi.front().dir_img = DIR_SX;
            break;
        case mov_dx:
            dati.personaggi.front().in_movim_dx = true;
            dati.personaggi.front().in_movim_sx = false;
            dati.personaggi.front().direzione = DX;
            dati.personaggi.front().dir_img = DIR_DX;
            break;
        case salto:
            if(!dati.personaggi.front().cade && !pausa)
                dati.personaggi.front().in_salto = true;
            break;
        case att_melee:
            if(!pausa)
                attacca(dati.personaggi.front());
            break;
        case att_dist:
            if(!pausa)
                spara(dati, dati.personaggi.front());
            break;
        case esc:
            return true;
        case invio:
            if(pausa)
                fuori_pausa(eventi, pausa);
            else
                in_pausa(eventi, pausa);
            
            break;
        /*case fr_su:
            
            break;
        case fr_giu:
            
            break;
        case fr_sx:
            
            break;
        case fr_dx:
            
            break;*/
    }
    return false;
}

void gestore_tasto_su(dati_eventi &eventi, ALLEGRO_EVENT *evt, dati_gioco &dati, bool &pausa)
{
    switch(evt->keyboard.keycode)
    {
        case mov_sx:
            dati.personaggi.front().in_movim_sx = false;
            break;
        case mov_dx:
            dati.personaggi.front().in_movim_dx = false;
            break;
        /*case salto:
            break;
        case att_melee:
        
            break;
        case att_dist:
            
            break;
        case esc:
        
            break;
        case invio:
        
            break;
        case fr_su:
        
            break;
        case fr_giu:
        
            break;
        case fr_sx:
        
            break;
        case fr_dx:
        
            break;*/
    }
}

dati_eventi inizializza_dati_eventi(int fps, int an_fps, int movpersps, int aggps, int movoggps,
                                        int vel_salto, int gennempm, int genarmpgpm, int genarmpngpm, 
                                        int actps)
{
    dati_eventi eventi;
    eventi.coda_eventi = al_create_event_queue();
    eventi.timer_fps = al_create_timer(1.0/fps);
    eventi.timer_anim = al_create_timer(1.0/an_fps);
    eventi.timer_mov_pers = al_create_timer(1.0/movpersps);
    eventi.timer_mov_ogg = al_create_timer(1.0/movoggps);
    eventi.timer_agg = al_create_timer(1.0/aggps);
    eventi.timer_salto = al_create_timer(1.0/vel_salto);
    eventi.timer_gen_nemici = al_create_timer(60.0/gennempm);
    eventi.timer_gen_armi_pg = al_create_timer(60.0/genarmpgpm);
    eventi.timer_gen_armi_png = al_create_timer(60.0/genarmpngpm);    
    eventi.timer_ia = al_create_timer(1.0/actps);

    return eventi;
}