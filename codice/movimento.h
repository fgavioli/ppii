#include <list>
#include "dati.h"
#include "eventi.h"

using namespace std;

const int MAX_VEL_X =  6;   /**< Costante che indica l'accelerazione massima dei personaggi sull'asse X   */
const int MIN_VEL_X = -6;   /**< Costante che indica la decelerazione massima dei personaggi sull'asse X  */
const int MAX_VEL_Y =  3;   /**< Costante che indica l'accelerazione massima dei personaggi sull'asse Y (Non implementata) */
const int MIN_VEL_Y = -3;   /**< Costante che indica la decelerazione massima dei personaggi sull'asse Y (Non implementata) */

/** Funzione che aggiorna l'accelerazione sull'asse x del personaggio principale
 */
void aggiorna_velocita_x(dati_gioco &dati);

/** Funzione che aggiorna l'accelerazione sull'asse y del personaggio principale
 * (NON IMPLEMENTATA)
 */
void aggiorna_velocita_y(dati_gioco &dati);

/** Funzione che aggiorna velocità x del personaggio
 * passato in input verso il personaggio principale.
 * NON PIU' SUPPORTATA, NUOVA IA SU ::ia.cc
 */
void muovi_png(personaggio &p);

/** Funzione che gestisce l'accelerazione in salto e la decelerazione
 * durante la caduta dei personaggi.
 */
bool salta(const dati_schermo &schermo, list<oggetto> &oggetti, list<personaggio> &personaggi);

/** Funzione che muove tutti i personaggi all'interno del gioco, e 
 * nel frattempo controlla le collisioni con altri personaggi e 
 * oggetti in gioco, ritorna 0 se tutto è andato a buon fine, true 
 * se si verifica la condizione di fine gioco (Personaggio principale
 * con 0 punti vita).
 */
bool muovi_personaggi(const dati_schermo &schermo, list<personaggio> &personaggi, list<oggetto> oggetti);

/** Funzione che muove tutti gli oggetti all'interno del gioco, e 
 * nel frattempo controlla le collisioni con altri personaggi e 
 * oggetti in gioco, ritorna 0 se tutto è andato a buon fine, true 
 * se si verifica la condizione di fine gioco (Personaggio principale
 * con 0 punti vita).
 */
bool muovi_oggetti(const dati_schermo &schermo, list<oggetto> &oggetti, list<personaggio> &personaggi);

/** Funzione che muove, fa attaccare i nemici presenti
 * su schermo.
 */
void esegui_azione(dati_gioco &dati, personaggio &p);