#include "statistiche.h"

statistiche inizializza_statistiche()
{
    statistiche ret;
    ret.nemici_eliminati = 0;
    ret.oggetti_lanciati_pg = 0;
    ret.durata_partita = 0;
    ret.danni_ricevuti_cmp = 0;
    ret.danni_ricevuti_spada = 0;
    return ret;
}

double stats(statistiche stat, int i)
{
    switch(i)
    {
        case 0:
            return static_cast<double>(stat.nemici_eliminati);
        case 1:
            return static_cast<double>(stat.oggetti_lanciati_pg);
        case 2:
            return stat.durata_partita;
        case 3:
            return static_cast<double>(stat.danni_ricevuti_cmp);
        case 4:
            return static_cast<double>(stat.danni_ricevuti_spada);
        case 5:
            return static_cast<double>(stat.punteggio);
        
        default:
            return 0;
    }
}

void calcola_punteggio(statistiche &stat)
{
    stat.punteggio = 0;
    stat.punteggio += stat.nemici_eliminati                     * PT_PER_ELM;
    stat.punteggio += stat.danni_ricevuti_cmp                   * PT_PER_DAN_CMP;
    stat.punteggio += stat.danni_ricevuti_spada                 * PT_PER_DAN_SPD;
    stat.punteggio += static_cast<int>(stat.durata_partita+1)   * PT_PER_SEC;
}
