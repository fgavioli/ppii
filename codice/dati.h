#include <list>
#include <iostream>
#include <unistd.h>
#include <cassert>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "statistiche.h"

using namespace std;

#ifndef STRUTTURE_DATI
#define STRUTTURE_DATI

const int ALT_SPRITE        = 100;      /**< Altezza di ogni sprite in gioco*/
const int LARG_SPRITE       =  50;      /**< Larghezza di ogni sprite in gioco*/

const int ALT_OGG           =  12;      /**< Altezza di ogni arma da lancio in gioco*/
const int LARG_OGG          =  50;      /**< Larghezza di ogni arma da lancio in gioco*/

const int VEL_OGG_MELEE     =   3;      /**< Velocità fissa di ogni arma ravvicinata in gioco*/
const int VEL_OGG_RANGED    =  10;      /**< Velocità fissa di ogni arma da lancio in gioco*/

const int PG_OGG            =   2;      /**< Numero massimo di armi da lancio per il personaggio giocante*/
const int NPC_OGG           =   1;      /**< Numero massimo di armi da lancio per il personaggio lanciere*/

const int OFFSET_COLL       =   5;      /**< Valore che delimita di quanti pixel un oggetto deve compenetrare un altro per causare la collisione*/
const int OFFSET_LARG_MELEE =  35;      /**< Valore che esprime di quanti pixel deve essere spostata l'arma rispetto alla x del personaggio*/
const int OFFSET_ALT_MELEE  =  40;      /**< Valore che esprime di quanti pixel deve essere spostata l'arma rispetto alla y del personaggio*/
const int MAX_FASI_ATTACCO  =  18;      /**< Valore che indica quante fasi d'attacco vengno eseguite, un numero più alto porta ad attaccare più lontano*/

const int LARG_IMG_DIR      =  10;      /**< Valore che indica la larghezza in pixel dell'immagine di freccia direzionale*/
const int OFFSET_IMG_DIR    =  15;      /**< Valore che indica la distanza tra il bordo del personaggio e la freccia direzionale*/

const int PG_HP   = 10;               /**< Punti vita totali personaggio principale*/
const int SPDC_HP = 1;                /**< Punti vita totali spadaccino*/
const int INSG_HP = 1;                /**< Punti vita totali inseguitore*/
const int TANK_HP = 2;                /**< Punti vita totali tank*/

struct informazioni_disegno;
struct struttura;
struct effetto;
struct oggetto;
struct personaggio;

/** Enumeratore che indica la direzione verso la quale 
 * ogni entità in gioco è girata.
*/
enum direzione_t {SX, DX};

/** Enumeratore che indica il tipo di personaggio in gioco
 * Principale è il personaggio giocante, 
 * Spadaccino è un nemico che si avvicina e attacca con la spada,
 * Inseguitore è un nemico che si muove a velocità maggiorata,
 * Tank è un nemico che si muove lentamente, ma infligge 2 danni
 * al personaggio con ogni spadata.
 */
enum tipo_t {PRINCIPALE, SPADACCINO, INSEGUITORE, TANK};

/** Struttura che contiene tutte le informazioni
 * necessarie a disegnare lo sprite di un'entità
 * e la sua rotazione.
 */
struct informazioni_disegno
{
    ALLEGRO_BITMAP* immagine;   /**< Immagine da disegnare*/
    int rotazione;              /**< Espressa in gradi*/
    bool visibile;              /**< Se settata a false l'oggetto non viene disegnato*/
};

/** (NON IMPLEMENTATO)
struct struttura
{
    informazioni_disegno draw;
    bool visibile;
};

 */
/** (NON IMPLEMENTATO)
struct effetto
{
    informazioni_disegno draw;
};
*/

/**Struttura che descrive un qualsiasi oggetto in gioco,
 * in questa implementazione, un'arma da lancio
*/
struct oggetto
{
    personaggio *padre;             /**< Puntatore al personaggio che ha generato questo oggetto*/
    int x;                          /**< Posizione sull'asse delle ascisse dell'oggetto */
    int y;                          /**< Posizione sull'asse delle ordinate dell'oggetto*/
    int vel_x;                      /**< Velocità sull'asse x dell'oggetto*/
    int vel_y;                      /**< Velocità sull'asse y dell'oggetto*/
    int fase_attacco;               /**< Indica quanti incrementi alla x dell'oggetto sono stati eseguiti*/
    bool in_uscita;                 /**< Indica se l'arma è in uscita (solo per oggetto arma ravvicinata)*/
    bool rim;                       /**< Indica se l'arma deve essere rimossa*/
    direzione_t direzione;          /**< Direzione di movimento dell'oggetto*/
    informazioni_disegno sprite;    /**< Informazioni di disegno dell'oggetto*/
};

/**Struttura che descrive un qualsiasi 
 * personaggio in gioco.
 */
struct personaggio
{
    int hp_totali;                  /**< Numero di vite massime di cui il personaggio può disporre*/
    int x;                          /**< Posizione del personaggio sull'asse delle ascisse*/
    int y;                          /**< Posizione del personaggio sull'asse delle ordinate*/
    int vel_x;                      /**< Velocità del personaggio sull'asse delle ascisse*/
    int vel_y;                      /**< Velocità del personaggio sull'asse delle ordinate*/
    int num_oggetti;                /**< Numero di oggetti di cui il personaggio dispone*/
    bool cade;                      /**< Flag che indica se il personaggio è in fase di caduta*/
    bool in_salto;                  /**< Flag che indica se il personaggio è in fase di salto*/
    bool in_movim_dx;               /**< Flag che indica se il personaggio è in movimento verso destra*/
    int hp;                         /**< Numero di vite di cui il personaggio dispone in un determinato tempo*/
    bool in_movim_sx;               /**< Flag che indica se il personaggio è in movimento verso sinistra*/
    bool in_attacco;                /**< Flag che indica se il personaggio sta svolgendo un attacco ravvicinato*/
    bool rim;                       /**< Flag utile alla rimozione dell'oggetto */
    ALLEGRO_BITMAP* dir_img;        /**< Immagine (freccia) che descrive la direzione di movimento del personaggio*/
    oggetto arma;                   /**< Arma a distanza ravvicinata del personaggio*/
    tipo_t tipo;                    /**< Enumeratore che indica il tipo di personaggio (Princpale-Spadaccino-Lanciere-Inseguitore)*/
    direzione_t direzione;          /**< Direzione in cui il personaggio è voltato*/
    informazioni_disegno sprite;    /**< Informazioni di disegno del personaggio*/
};

/**Struttura dati che contiene diverse liste, a loro volta 
 * contenenti tutti i dati necessari all'infrastruttura del
 * gioco per funzionare correttamente.
 **/
struct dati_gioco
{
//    list<struttura> strutture;      /**< Lista contenente strutture su cui il personaggio può salire (Non implementato)*/
//    list<effetto> effetti_retro;    /**< Lista contenente effetti grafici attivi dietro ai personaggi (Non implementato)*/
//    list<effetto> effetti_fronte;   /**< Lista contenente effetti grafici attivi davanti ai personaggi (Non implementato)*/
    list<oggetto> oggetti;          /**< Lista contenente tutte le armi da lancio attive nello scenario*/
    list<personaggio> personaggi;   /**< Lista contenente tutti i personaggi attivi nello scenario*/
};
#endif

/** Funzione che genera un nuovo Personaggio Non Giocante
 * in una posizione casuale, e lo mette
 * in coda nella lista "personaggi"
 */
void genera_png(dati_gioco &dati, int y_terreno);

/** Funzione di inizializzazione che ritorna una struttura di tipo personaggio
 * riempito in base ai parametri forniti.
*/
personaggio inizializza_personaggio(int x, int y, int rotazione, tipo_t tipo, direzione_t dir);

/** Funzione di inizializzazione che ritorna una struttura di tipo personaggio
 * riempita in base ai parametri forniti.
 */
oggetto inizializza_oggetto(int x, int y, int vel_x, int vel_y, direzione_t dir, personaggio &p_id, ALLEGRO_BITMAP *sprite);

/** Funzione di attacco, rende visibile l'arma e fa partire il movimento.
 */
void attacca(personaggio &p);

/** Funzione che prende in ingresso i dati ed un personaggio (per riferimento)
 * Inserisce nella lista oggetti un nuovo oggetto opportunamente inizializzato
 * attraverso direzione e velocità in asse x.
*/
void spara(dati_gioco &dati, personaggio &p);

/**Funzione che controlla a coppie se due personaggi collidono,
 * nel caso succedesse, toglie una vita ad entrambi i personaggi
 * coinvolti, e se la vita raggiunge 0, li elimina dalla lista.
 * Ritorna true se il personaggio giocante raggiunge 0 punti vita.
*/
bool risolvi_collisioni_pg_pg(list<personaggio> &personaggi);

/**Funzione che controlla se un personaggio ed un'arma (da lancio)
 * collidono, nel caso succedesse, toglie una vita al personaggio e
 * elimina l'arma dal gioco, se il personaggio coinvolto raggiunge 
 * il valore di vita 0, lo elimina dalla lista.
 * Ritorna true se il personaggio giocante raggiunge 0 punti vita
*/
bool risolvi_collisioni_ogg_pg(list<oggetto> &oggetti, list<personaggio> &personaggi);

/**Funzione che controlla a coppie se due oggetti collidono,
 * nel caso succedesse, elimina dalla lista entrambi gli oggetti.
 * (IMPLEMENTATA, FUNZIONANTE MA INUTILIZZATA)
*/
void risolvi_collisioni_ogg_ogg(list<oggetto> &oggetti);