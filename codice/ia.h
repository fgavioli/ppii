#include "dati.h"
#include "movimento.h"
#include <math.h>

const int RANGE_ATT_SPDC    = 150;
const int RANGE_ATT_TANK    = 100;
const int RANGE_VISIONE     = 115;
const int RANGE_SALTO_INSG  = 250;

/** Funzione che esegue l'intelligenza del personaggio
  * spadaccino
*/
void ia_spadaccino(dati_gioco &dati, personaggio &p);

/** Funzione che esegue l'intelligenza del personaggio 
  * inseguitore. 
*/
void ia_inseguitore(dati_gioco &dati, personaggio &p);

/** Funzione che esegue l'intelligenza del personaggio 
  * tank. 
 */
void ia_tank(dati_gioco &dati, personaggio &p);