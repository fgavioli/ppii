#include "grafica.h"
#include <allegro5/allegro.h>
#include <iostream>

using namespace std;

#ifndef TASTI
#define TASTI

const int mov_sx    = ALLEGRO_KEY_A;        /**< Tasto di movimento a sinistra*/
const int mov_dx    = ALLEGRO_KEY_D;        /**< Tasto di movimento a destra*/
const int salto     = ALLEGRO_KEY_SPACE;    /**< Tasto di salto*/
const int att_melee = ALLEGRO_KEY_J;        /**< Tasto per attacco a breve distanza*/
const int att_dist  = ALLEGRO_KEY_K;        /**< Tasto di lancio arma a lunga distanza*/
const int esc       = ALLEGRO_KEY_ESCAPE;   /**< Tasto di terminazione del gioco*/
const int invio     = ALLEGRO_KEY_ENTER;    /**< Tasto di pausa*/
const int fr_su     = ALLEGRO_KEY_UP;       /**< (Non implementato)*/
const int fr_giu    = ALLEGRO_KEY_DOWN;     /**< (Non implementato)*/
const int fr_sx     = ALLEGRO_KEY_LEFT;     /**< (Non implementato)*/
const int fr_dx     = ALLEGRO_KEY_RIGHT;    /**< (Non implementato)*/

/** Struttura che gestisce le informazioni relative
 * agli eventi che vengono generati dagli oggetti
 * ALLEGRO. Viene riempita da ::inizializza_dati_eventi()
 */
struct dati_eventi
{
    ALLEGRO_EVENT_QUEUE *coda_eventi;   /**< Oggetto delle librerie Allegro che memorizza la coda dove vengono inseriti gli eventi (FIFO)*/
    ALLEGRO_TIMER *timer_fps;           /**< Timer che genera un evento di aggiornamento dello schermo*/
    ALLEGRO_TIMER *timer_anim;          /**< Timer che genera un evento di animazione (Non implementato)*/
    ALLEGRO_TIMER *timer_mov_pers;      /**< Timer che genera un evento di movimento dei personaggi*/
    ALLEGRO_TIMER *timer_mov_ogg;       /**< Timer che genera un evento di movimento degli oggetti*/
    ALLEGRO_TIMER *timer_salto;         /**< Timer che genera un evento di movimento per salto*/
    ALLEGRO_TIMER *timer_agg;           /**< Timer che genera un evento di aggiornamento accelerazioni personaggi*/
    ALLEGRO_TIMER *timer_gen_nemici;    /**< Timer che genera un evento di generazione di un nuovo nemico*/
    ALLEGRO_TIMER *timer_gen_armi_pg;   /**< Timer che genera un evento di incremento (di una unità) di ::personaggio::num_oggetti nel personaggio principale*/
    ALLEGRO_TIMER *timer_gen_armi_png;  /**< Timer che genera un evento di incremento (di una unità) di ::personaggio::num_oggetti nei nemici*/
    ALLEGRO_TIMER *timer_ia;            /**< Timer che genera un evento di azione ia.*/
};

#endif

/** Registra le sorgenti degli eventi (timers e schermo)
 * all'interno della coda di eventi ::eventi.coda_eventi.
 */
void registra_sorgenti_eventi(dati_eventi &eventi, dati_schermo &schermo);

/** Funzione che ferma tutti i timer, di fatto
  * mettendo in pausa il gioco.
  * Parametro pausa: lo stato di pausa del gioco.
  * true->in pausa, false->in gioco
  */
void in_pausa(dati_eventi &eventi, bool &pausa);

/** Funzione che fa ripartire i timer da una situazione
  * di pausa, facendo ripartire il gioco.
  * Parametro pausa: lo stato di pausa del gioco.
  * true->in pausa, false->in gioco
  */
void fuori_pausa(dati_eventi &eventi, bool &pausa);

/** Funzione che fa partire tutti i timer di inizio
 * gioco.
 */
void start_timers(dati_eventi &eventi);

/** Funzione che ferma tutti i timer di gioco.
  */
void stop_timers(dati_eventi &eventi);

/** Funzione che gestisce la pressione di un bottone su tastiera
 * Da chiamare una volta generato l'evento  ALLEGRO_EVENT_KEY_DOWN
 * Ritorna true se il pulsante ESC è stato premuto (uscita del gioco)
 */
bool gestore_tasto_giu(dati_eventi &eventi, ALLEGRO_EVENT *evt, dati_gioco &dati, bool &pausa);

/** Funzione che gestisce il rilascio di un bottone su tastiera
 * Da chiamare una volta generato l'evento ALLEGRO_EVENT_KEY_UP
 */
void gestore_tasto_su(dati_eventi &eventi, ALLEGRO_EVENT *evt, dati_gioco &dati, bool &pausa);

/** Funzione che inizializza un nuovo oggetto di tipo dati_eventi,
 * contestualmente ai parametri passati in input.
 */
dati_eventi inizializza_dati_eventi(int fps, int an_fps, int movpersps, int aggps, int movoggps,
                                        int vel_salto, int gennempm, int genarmpgpm, int genarmpngpm, 
                                        int actps);