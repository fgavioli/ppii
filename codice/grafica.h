#include "dati.h"
#include "fileIO.h"
#include <list>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

using namespace std;

#ifndef DATI_SCHERMO
#define DATI_SCHERMO

const int Y_TERRENO         = 705;              /**< Posizione del terreno sull'asse y dove scorrono i personaggi*/
const int MAX_ALT_SALTO     = Y_TERRENO - 375;  /**< Altezza massima di salto*/
const int MED_ALT_SALTO_3   = Y_TERRENO - 350;  /**< Altezza alla quale la velocità di salita-discesa del salto viene ridotta-aumentata*/
const int MED_ALT_SALTO_2   = Y_TERRENO - 325;  /**< Altezza alla quale la velocità di salita-discesa del salto viene ridotta-aumentata ulteriormente*/
const int MED_ALT_SALTO_1   = Y_TERRENO - 250;  /**< Altezza alla quale la velocità di salita-discesa del salto viene ridotta-aumentata ancora una volta*/
const int X_HUD             = 40;               /**< Distanza tra il margine sinistro dello schermo e le immagini dell'hud*/
const int X_TESTO_HUD       = X_HUD + 60;       /**< Distanza tra il margine sinistro dello schermo e il testo dell'hud*/
const int Y_OFFSET_HUD      = 35;               /**< Distanza tra il margine superiore dello schermo e la prima riga dell'hud*/
const int Y_INTERLINEA      = 60;               /**< Distanza tra una riga e la successiva*/

/** Struttura contenente i dati dello schermo, necessari per
 * l'aggiornamento della schermata
 */
struct dati_schermo
{
    ALLEGRO_DISPLAY *display;           /**< Oggetto delle librerie Allegro che contiene le informazioni del display */
    ALLEGRO_BITMAP *sfondo;             /**< Immagine di sfondo del gioco */
    ALLEGRO_BITMAP *nvite;              /**< Immagine delle vite rimanenti */
    ALLEGRO_BITMAP *narmi;              /**< Immagine delle armi rimanenti */
    ALLEGRO_BITMAP *nnpc;               /**< Immagine delle npc vive rimanenti */
    ALLEGRO_FONT *hud_font;             /**< Il font utilizzato per la stampa degli elementi sull'interfaccia */
    int larghezza;                      /**< Larghezza del disegno*/
    int altezza;                        /**< Altezza del disegno*/
    int fps;                            /**< Aggiornamenti per secondo dello schermo*/
    bool serve_disegno;                 /**< Flag che indica che lo schermo deve essere ridisegnato*/
};

#endif

/** Funzione di inizializzazione dello schermo, contestualmente
 * ai parametri passati in input.
 */
bool inizializza_schermo(dati_schermo &schermo, int larg_disegno, int alt_disegno, int fps, ALLEGRO_BITMAP *img_sfondo,
                            ALLEGRO_BITMAP* img_hp_hud, ALLEGRO_BITMAP* img_col_hud, ALLEGRO_BITMAP* img_npc_hud,
                            const char *hud_path, int hud_font_size);

/** (NON IMPLEMENTATA)
 */
void avanza_animazioni(dati_schermo &schermo, dati_gioco &gioco);

/** Stampa dell'immagine di sfondo
 */
void stampa_sfondo(const ALLEGRO_BITMAP* immagine);

/*
 * Stampa di strutture su schermo (NON IMPLEMENTATA)
 */
void stampa_strutture(const list<struttura> strutture);

/*
 * Stampa di effetti in background su schermo (NON IMPLEMENTATA)
 */
void stampa_effetti_retro(const list<effetto> effetti_retro);

/** Stampa di tutta la lista oggetti su schermo.
 */
void stampa_oggetti(const list<oggetto> oggetti);

/** Stampa di tutta la lista personaggi su schermo.
 */
void stampa_personaggi(const list<personaggio> personaggi);

/*
 * Stampa di effetti in foreground su schermo (NON IMPLEMENTATA)
 */
void stampa_effetti_fronte(const list<effetto> effetti_fronte);

/** Funzione che stampa a schermo i dati relativi alla partita:
 *      - Vita del giocatore
 *      - Numero di coltelli in inventario
 *      - Numero di nemici eliminati
*/
void stampa_interfaccia_dati(const dati_gioco &dati, const dati_schermo &schermo);

/** Funzione che utilizza tutte le stampre con un preciso ordine 
 * di esecuzione, allo scopo di generare correttamente
 * i diversi livelli di sovrapposizione delle immagini
 */
void stampa_fotogramma(dati_schermo &schermo, const dati_gioco &dati);

/** Funzione che crea una schermata per la lettura di
  * tre caratteri, rappresentanti il nome da salvare 
  * nella lista dei punteggi migliori, ritorna puntatore
  * a caratteri contenente il nome.
  */
char* leggi_nome(dati_schermo &schermo);

/** Funzione che stampa a schermo il riepilogo di fine gioco, 
  * che riassume tutte le attività importanti e il punteggio
  * finale della partita.
  */
void stampa_risultati(dati_schermo schermo);

/** Funzione che stampa a schermo la lista dei 10
  * punteggi migliori, caricati attraverso ::leggi_highscores.
  * Il parametro h è un puntatore di tipo highscore, se passato NULL,
  * la funzione stampa solo a schermo la lista, se invece è 
  * inizializzato, viene inserito nella lista dei punteggi ed
  * evidenziato colorandolo di verde.
  */
void stampa_highscores(highscore *h);